#include <iostream>
using namespace std;

class Expression
{
public:
    Expression()
    {
        ++instances;
    };
    friend ostream &operator<<(ostream &out, const Expression &exp)
    {
        return exp.affiche(out);
    };
    virtual std::ostream &affiche(std::ostream &out) const 
    { 
        return out; 
    };
    virtual Expression *derive(const string n) const = 0;
    virtual Expression *clone() const = 0;


    virtual ~Expression()
    {
        --instances;
    };

    static int nombre_instances()
    {
        return instances;
    }
private:
    static int instances;
};

int Expression::instances{0};

class Nombre : public Expression
{
public:
    Nombre()
    {
        valeur = 0;
    };
    Nombre(const long int &n)
    {
        valeur = n;
    };
    std::ostream &affiche(std::ostream &out) const override
    {
        return out << valeur;
    };
    Expression *derive(const string n) const
    {
        return new Nombre(0);
    };
    Expression *clone() const override
    {
        return new Nombre(valeur);
    };


    friend ostream &operator<<(ostream &out, const Nombre &n)
    {
        out << n.valeur;
        return out;
    };
    ~Nombre(){};

private:
    int valeur;
};

class Variable : public Expression
{
public:
    Variable()
    {
        nom = "";
    };
    Variable(const string &n)
    {
        nom = n;
    };

    std::ostream &affiche(std::ostream &out) const override
    {
        return out << nom;
    };
    Nombre *derive(string n) const
    {
        if (nom == n)
        {
            return new Nombre(1);
        }
        else
        {
            return new Nombre(0);
        }
    };
    Expression *clone() const override
    {
        return new Variable(nom);
    };

    

    ~Variable(){};

    string nom;
};

class Operation : public Expression
{
public:
    Operation(){};
    ~Operation(){};
protected:
    Expression *gauche;
    Expression *droite;
};

class Addition : public Operation
{
public:
    Addition(Expression *g, Expression *d)
    {
        gauche = g;
        droite = d;
    };
    std::ostream &affiche(std::ostream &out) const override
    {
        return out << "(" << *gauche << " + " << *droite << ")";
    };

    Expression *derive(const string n) const override
    {
        Expression *derive_gauche = (*gauche).derive(n);
        Expression *derive_droite = (*droite).derive(n);

        return new Addition(derive_gauche, derive_droite);
    };
    Expression *clone() const override
    {
        return new Addition(gauche, droite);
    };


    ~Addition(){};
};

class Multiplication : public Operation
{
public:
    Multiplication(Expression *g, Expression *d)
    {
        gauche = g;
        droite = d;
    };
    std::ostream &affiche(std::ostream &out) const override
    {
        return out << "(" << *gauche << " * " << *droite << ")";
    };

    Expression *derive(const string n) const override
    {
        Expression *gauche_copy = (*gauche).clone();
        Expression *droite_copy = (*droite).clone();
        Expression *derive_gauche = (*gauche).derive(n);
        Expression *derive_droite = (*droite).derive(n);

        Expression *pdt_gauche = new Multiplication(derive_gauche,droite_copy);
        Expression *pdt_droite = new Multiplication(gauche_copy,derive_droite);
        return new Addition(pdt_gauche,pdt_droite);
    };
    Expression *clone() const override
    {
        return new Multiplication(gauche, droite);
    };


~Multiplication(){};
};

